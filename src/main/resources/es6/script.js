// Keyword let
// Introduced in ES2015 (aka ES6), the variable type let shares a lot of similarities with var but unlike var has scope constraints.
// let is constrained to whichever scope it is declared in. Its declaration and assignment are similar to var
// ATTENTION: uncomment to see error

// let x = 20;
// let x = 50; // SyntaxError: identifier "x" has already been declared.
// compare with:
// var x = 20;
// var x = 50;

// if(10 > 5){
//     var x = 10;
// }
//
// console.log(x);

const multiplyES6 = (a, b) => { return a * b };
console.log(multiplyES6());

// spread operator
let arr1 = ["A", "B", "C"];
let arr2 = ["X", "Y", "Z"];
let result = [...arr1, ...arr2]; // merge of arrays
console.log(result);


const myArray = [`🤪`,`🐻`,`🎌`]
const yourArray = [`🙂`,`🤗`,`🤩`]
const ourArray = [...myArray,...yourArray]
console.log(...ourArray);

// Object destructuring
const obj = { hello: '1', world: '2' };
const { hello, world } = obj; // destructed object
console.log(world);


[a, b, ...rest] = [10, 20, 30, 40, 50];
// [a, b, rest] = [10, 20, 30, 40, 50]; // compare this
// [a, ...b, rest]; // SyntaxError: Rest element must be last element
console.log(a);
console.log(b);
console.log(rest);
