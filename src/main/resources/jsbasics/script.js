// singleline comments
/* multiline comments */
// console.log("__ TOPIC: DECLARING VARIABLES __");
//
// var text1 = "abc"; // same as in Java: string text1 = "abc";
// var text2 = 'abc';
// console.log("Concatenating strings: " + text1 + "-" + text2);
//
// var aggregate = 5 + text1 + "-" + text2 + "-something-else";
// console.log(aggregate);
//
// var number1 = 5;
// var number2 = 5.012;
// var number3 = 500000000000.012;
//
// // console.log("Adding numbers: " + number1 + " + " + number2 + " = " + number1 + number2); // this will not work
// console.log("Adding numbers: " + number1 + " + " + number2 + " = " + (number1 + number2));
// console.log(number3 + 0.1);

console.log(""); console.log("__ TOPIC: BASIC OPERATORS __");

var boolean1 = true;
var boolean2 = false;
console.log(">>" + 1 % 2); // prints 1
console.log(">>" + (boolean1 || boolean2)); // prints what?

// == VS ===
// In JavaScript there are two comparison operators. If
//  we want to compare values we use == operator. If we
//  want to compare values and types we use ===.
console.log(5 == 5); // prints true
console.log("5" == 5); // prints true
console.log("5" === 5); // prints false (types differ)

console.log(""); console.log("___ TOPIC: Conditionals ___");
var first = 10;
var second = 20;

if(first > second){
    console.log("First is bigger than second!");
} else {
    console.log("First is smaller than second!");
}


console.log(""); console.log("___ TOPIC: Loops ___");

for(var i = 0; i < 3; i++) {
    console.log("Inside for() loop, i is now:" + i);
}

var i = 0;
while(i < 3) {
    console.log("Inside while() loop, i is now:" + i);
    i++;
}


console.log(""); console.log("___ TOPIC: Arrays ___");

var myAwesomeArray = [1, 4, 7];
console.log(myAwesomeArray[2]); // prints 7, since arrays are 0 based
console.log(myAwesomeArray.length); // prints 3
console.log(myAwesomeArray[myAwesomeArray.length - 1]); // gets the last element

myAwesomeArray.push(9); // arrays are dynamic so we can extend them with additional elements
console.log(myAwesomeArray.length); // prints 4


// Can we have an array of mixed datatypes?
var mixedArray = ["A", 1, "Boo", true, ["Array", "Array"], {type: "object"}];
for(var i = 0; i < mixedArray.length; i++) {
    console.log("Type of " + mixedArray[i] + " = " + (typeof mixedArray[i]));
}

// arrays have a lot of functionality associated, see: https://www.w3schools.com/jsref/jsref_obj_array.asp
// ... like map, filter, reduce (what JAVA's streams provide as well)
var personel = [
    {
        id: 51,
        name: "Joana",
        baseSalary: 450,
        bonus: true,
        gender: "female"
    },
    {
        id: 2,
        name: "Jonas",
        salary: 600,
        bonus: false,
        gender: "male"
    },
    {
        id: 432,
        name: "Marta",
        salary: 1005,
        bonus: true,
        gender: "female"
    },
    {
        id: 44,
        name: "Petras",
        salary: 450,
        bonus: true,
        gender: "male"
    }
];

var males = personel.filter(function (person) {
    if (person.gender === "male")
        return person;
});

console.log("Male personnel:")
console.log(males);


console.log(""); console.log("___ TOPIC: Functions ___");

// inbuilt functions:
// https://www.w3schools.com/jsref/jsref_obj_global.asp

// function declaration
function myFunction(firstParam, secondParam){
    return firstParam + secondParam;
}

// function invocation - calling the function
console.log(myFunction(1, 2))
console.log(myFunction("a", "b"))


console.log(""); console.log("___ TOPIC: Objects ___");
// Object literal - one way to create objects
var cat = {
    weight: 12.2,
    name: "Fluffy",
    fur: false
};

// Two ways to access the properties
console.log(cat.weight);
console.log(cat.name);
console.log(cat.fur);


// Another way to access properties. This
// way is useful because you can use valid strings
// that are NOT VALID variable names to give to your objects
// maybe you received them from user input or some file
console.log(cat["weight"]);
console.log(cat["name"]);
console.log(cat["fur"]);

// We can add a property to our object afterwards
cat.age = 4;
console.log(cat.age);
console.log(cat);


console.log(Object.keys(cat));


// Adding functions / behavior to our objects
// ... we can also do it after we created them
// ... this is very hard to do in java, but in
// ... dynamically typed languages it is easy
cat.speak = function(){
    console.log("Meow!")
};
// cat.speak();

// Or we can add it to our object literal
var cat = {
   weight: 12.2,
   name: "Flufy",
   fur: false,
   speak: function(timesToSpeak) {
       for(var i = 0; i < timesToSpeak; i++)
           console.log("Meow!")
   }
};
cat.speak(6);
