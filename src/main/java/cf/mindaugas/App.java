package cf.mindaugas;

import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

import static spark.Spark.*;

public class App {
    public static void main(String[] args) {
        // curl -X GET http://localhost:4567/ -s
        get("/", (req, res) -> {
            return
            "<html>" +
                "<head>" +
                "</head>" +
                "<body>" +
                    "<a href=\"/form\">Go to form page!</a>" +
                "</body>" +
            "</html>";
        });

        // curl -X GET http://localhost:4567/hello -s
        get("/hello", (req, res) -> "Spark works!");

        // Return HTML from file
        get("/form", (req, res) -> {
            System.out.println("We reached the form w/ IP: " + req.ip());
            System.out.println("Request body: " + req.body());
            // Reference to the HTML file containing the form
            String htmlFile = "target\\classes\\html\\index.html";
            // Converting the contents of the file
            String htmlContent = new String(Files.readAllBytes(Paths.get(htmlFile)), StandardCharsets.UTF_8);
            System.out.println(htmlContent);
            return htmlContent;
        });

        post("/form", (req, res) -> {
            // Senasis sprendimas
            // return "Hi " + URLDecoder.decode(req.body(), "UTF-8").split("=")[1];

            // Naujoji versija
            String[] kvPairs = URLDecoder.decode(req.body(), "UTF-8").split("&");
            return "Hi " + kvPairs[0].split("=")[1] + " your age is: " + kvPairs[1].split("=")[1];
        });
    }
}
